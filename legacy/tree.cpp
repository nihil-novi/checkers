#include "tree.h"
#include <iostream>

using namespace std;

node::node()
{
    state = NULL;
    lastPos = 0;
}

node::node(checkerboard* _state, bool w)
{
    state = _state;
    lastPos = 0;
    if (state->turn == w)
    {
        mark = -100000;
    }
    else
    {
        mark = 100000;
    }
}

node::node(const node &op)
{
    state = new checkerboard(*(op.state));
    lastPos = op.lastPos;
}

node::~node()
{
    for (int i = 0; i < children.size(); i++)
        delete children[i];
    delete state;
    // хмммм вот тут беда, нужно срегламетировать удаление
}

void node::discover(bool player)
{
    vector<checkerboard*> turns = state->getTurns();
    node* temp;
    for (int i = 0; i<turns.size(); i++)
    {
        temp = new node(turns[i], player);
        children.push_back(temp);
    }
}

node* node::getNextChild()
{
    if (lastPos >= children.size())
    {
        return NULL;
    }
    return children[lastPos++];
}


void and_or_tree::alpha_beta(int depth)
{
	int q = 0;
	vector<node*> way;
    way.push_back(root);
    int curDepth = 0;
    node *x = root;
    root->discover(player);
    node* temp = root->getNextChild();
    if (temp == NULL)
    {
        return;
    }
    root->resetPos();
    while (way.size() != 0)
    {
        if ((x = x->getNextChild()) == NULL)
        {
			while (x == NULL)
			{
				if (curDepth == 0 && x == NULL)
				{
					root->resetPos();
					return;
				}
				way.pop_back();
				curDepth--;
				if (curDepth == -1)
				{
					int t = 4;
				}
				x = way[way.size() - 1]->getNextChild();
				if (curDepth == 0 && x == NULL)
				{
					root->resetPos();
					return;
				}
			}
        }
        way.push_back(x);
        curDepth++;
        if (curDepth < depth)
            x->discover(player);
        if (curDepth >= depth)
        {
			int i;
			bool divide;
            way[way.size() - 1]->mark = way[way.size() - 1]->state->getDivide(player);
			q++;
            for (i = way.size() - 1; i >= 1; i--)
            {
                divide = way[i-1]->remark(way[i]->mark, i - 1);
                if (divide) break; 
            }
            if (divide)
            {
				int steps = way.size() - i;
				for(int j = 0; j < steps; j++)
                {
                    way.pop_back();
                    curDepth--;
                }
                x = way[way.size() - 1];
            }
            else
            {
                way.pop_back();
                curDepth--;
                x = way[way.size() - 1];
            }
        }
    }
    root->resetPos();
}

and_or_tree::and_or_tree()
{
    player = 0;
    root = NULL;
}

and_or_tree::and_or_tree(const and_or_tree &op)
{
    // Рекурсивное копирование всего и вся
}
and_or_tree::and_or_tree(checkerboard* state, int _depth)
{
    depth = _depth;
    player = state->turn;
    root = new node(state, state->turn);
    alpha_beta(depth);

}
and_or_tree :: ~and_or_tree()
{
    delete root;
    // Рекурсивное удаление всего и вся
}

bool node::remark(int i, int depth)
{
    if (depth % 2 == 0)
    {
        if (mark < i)
        {
            mark = i;
            return false;
			
        }
        else
        {
            return true;
        }
    }
    else
    {
        if (mark > i)
        {
            mark = i;
            return false;
        }
        else
        {
            return true;
        }
    }
}

checkerboard* and_or_tree::returnTurn()
{
    node* current;
    while ((current = root->getNextChild()) != NULL)
    {
        if (current->mark == root->mark)
        {
            checkerboard* temp = new checkerboard(*(current->state));
            return temp;
        }
    }
    root->resetPos();
	int max = -1000000;
	int id = 0;
	int i = 0;
	while ((current = root->getNextChild()) != NULL)
	{
		if (current->mark > max)
		{
			max = current->mark;
			id = i;
		}
		i++;
	}
	root->resetPos();
	for (int i = 0; i <= id; i++)
		current = root->getNextChild();
    checkerboard* temp = new checkerboard(*(current->state));
    return temp;
}

void node::resetPos()
{
    lastPos = 0;
}