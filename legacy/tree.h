#include "checkerboard.h"

class node
{
private:
	vector<node*> children;
    int lastPos;
public:
	node();
	node(checkerboard* _state,bool w);
	node(const node &op);
	~node();
	void discover(bool player);
    node* getNextChild();
	void  resetPos();
    int mark;
    checkerboard* state;
    bool remark(int i, int depth);
};

class and_or_tree
{
private:
	int depth;
	bool player; // 0 - WHITE, 1 - BLACK
	node* root;
public:
	and_or_tree();
	and_or_tree(const and_or_tree &op);
	and_or_tree(checkerboard* state, int _depth);
	~and_or_tree();
    void alpha_beta(int depth);
	checkerboard* returnTurn();
};