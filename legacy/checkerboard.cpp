﻿#include "checkerboard.h"

using namespace std;

#define canEat(a, b) (((a) == WHITEQUEEN) && (b) || ((a) == BLACKQUEEN) && !(b) || ((a) == WHITE) && (b) || ((a) == BLACK) && !(b))

checkerboard::checkerboard()
{
	turn = false;
	eat = false;
	for (int i = 0; i < 12; i++)
	{
		field[i] = WHITE;
	}
	for (int i = 12; i < 20; i++)
	{
		field[i] = EMPTINESS;
	}
	for (int i = 20; i < 32; i++)
	{
		field[i] = BLACK;
	}
}

checkerboard::checkerboard(const checkerboard &op)
{
	for (int i = 0; i < 32; i++)
		field[i] = op.field[i];
	turn = op.turn;
	eat = op.eat;
}

checkerboard::checkerboard(cell* _field, bool _turn)
{
	for (int i = 0; i < 32; i++)
		field[i] = _field[i];
	turn = _turn;
	eat = false;
}

vector<checkerboard*> checkerboard::getTurns()
{
	vector<checkerboard*> result;
	checkEat();
    vector<cell*> options;
	for (int i = 0; i < 32; i++)
	{
        if ( (field[i] == WHITE) && !turn || (field[i] == BLACK) && turn )
        {
            options = checkerTurn(i);
			for (unsigned int j = 0; j < options.size(); j++)
			{
				checkerboard* q = new checkerboard(options[j], !turn);
				result.push_back(q);
				delete[] options[j];
			}
            options.clear();
            continue;
        }
        if ( (field[i] == WHITEQUEEN) && !turn || (field[i] == BLACKQUEEN) && turn )
        {
            options = queenTurn(i, NULL);
			for (unsigned int j = 0; j < options.size(); j++)
			{
				checkerboard* q = new checkerboard(options[j], !turn);
				result.push_back(q);
				delete[] options[j];
			}
            options.clear();
        }
	}
    return result;
}

void checkerboard::checkEat()
{
	UCHAR pos1, pos2;
	for (int i = 0; i < 32; i++)
	{
        if ( (field[i] == WHITE) && !turn || (field[i] == BLACK) && turn )
        {
            for (int j = 0; j < 4; j++)
			{
				pos1 = link[i][j];
				if (pos1 == NONE)
					continue;
				pos2 = link[pos1][j];
				if (pos2 == NONE)
					continue;
				if ((field[pos1] == WHITE || field[pos1] == WHITEQUEEN) && 
					(field[pos2] == EMPTINESS) && turn ||
					(field[pos1] == BLACK || field[pos1] == BLACKQUEEN) &&
					(field[pos2] == EMPTINESS) && !turn)
				{
					eat = true;
					return;
				}
			}
        }
        if ( (field[i] == WHITEQUEEN) && !turn || (field[i] == BLACKQUEEN) && turn )
        {
            for (int j = 0; j < 4; j++)
			{
				pos1 = link[i][j];
				if (pos1 == NONE)
					continue;
				while (field[pos1] == EMPTINESS)
				{
					pos1 = link[pos1][j];
					if (pos1 == NONE)
						break;
				}
				if (pos1 == NONE)
					continue;
				pos2 = link[pos1][j];
				if (pos2 == NONE)
					continue;
				if ((field[pos1] == WHITE || field[pos1] == WHITEQUEEN) 
                    && (field[pos2] == EMPTINESS) && turn 
                    || (field[pos1] == BLACK || field[pos1] == BLACKQUEEN) 
                    && (field[pos2] == EMPTINESS) && !turn)
				{
					eat = true;
					return;
				}
			}
        }
	}
	eat = false;
}

vector<cell*> checkerboard::checkerTurn(UCHAR pos)
{
	vector<cell*> result;
	if (eat)
	{
        vector<WayPoint> way;
        way.push_back(WayPoint(pos, field[pos]));
        while (!way.empty())
        {
            UCHAR curDir = way[way.size()-1].getDir();
            if (curDir == 5 
                || (way.size() > 4) && (way[way.size() - 1].pos == way[way.size() - 5].pos)
                || (way.size() > 6) && (way[way.size() - 1].pos == way[way.size() - 7].pos))
            {
                if (way[way.size()-1].enemy == NONE && way.size() > 1)
                {
                    cell* temp = new cell[32];
                    memcpy(temp, field, sizeof(field));
                    for (int i = 0; i < way.size() - 1; i++)
                    {
                        temp[way[i].pos] = EMPTINESS;
                        temp[way[i].enemy] = EMPTINESS;
                    }
                    temp[way[way.size()-1].pos] = way[way.size()-1].type;
                    way.pop_back();
                    result.push_back(temp);
                }
                else
                {
                    way.pop_back();
                }
                continue;
            }
            UCHAR curEnemy = link[way[way.size()-1].pos][curDir];
            if (curEnemy == NONE)
            {
                continue;
            }
            if (way.size() > 1 && curEnemy == way[way.size() - 2].enemy)
            {
                continue;
            }
            UCHAR newPos = link[curEnemy][curDir];
            if (canEat(field[curEnemy], turn) && newPos != NONE && (field[newPos] == EMPTINESS))
            {
                bool isQueen = false;
                cell type = way[way.size()-1].type;
                if (newPos > 27 && !turn)
                {
                    type = WHITEQUEEN;
                    isQueen = true;
                }
                else if (newPos < 4 && turn)
                {
                    type = BLACKQUEEN;
                    isQueen = true;
                }
                way[way.size()-1].enemy = curEnemy;
                way.push_back(WayPoint(link[curEnemy][curDir], type));
                if (isQueen)
                {
                    vector<cell*> ret = queenTurn(link[curEnemy][curDir], &way);
                    while (!ret.empty())
                    {
                        result.push_back(ret[ret.size() - 1]);
                        ret.pop_back();
                    }
                }
            }
        }
	}
	else
	{
 		int i = ((int)!turn) * 2;
		int limit = i + 2;
		for (i; i < limit; i++)
		{
            UCHAR new_pos = link[pos][i];
			if (new_pos == NONE)
				continue;
			if (field[new_pos] != EMPTINESS)
				continue;
			cell *temp = new cell[32];
			memcpy(temp, field, sizeof(cell)*32);
			temp[new_pos] = field[pos];
			if ((new_pos > 27) && (field[pos] = WHITE))
				temp[new_pos] = WHITEQUEEN;
			if ((new_pos < 4) && (field[pos] = BLACK))
				temp[new_pos] = BLACKQUEEN;
			temp[pos] = EMPTINESS;
			result.push_back(temp);
		}
	}
	return result;
}

vector<cell*> checkerboard::queenTurn(UCHAR pos, vector<WayPoint> *way_)
{
	vector<cell*> result;
    if (eat)
    {
        int curDepth = 0;
        vector<WayPoint> way;
        if (way_ != NULL)
        {
            way = *(way_);
            curDepth = way_->size() - 1;
        }
        way.push_back(WayPoint(pos, field[pos]));
        while (way.size() > curDepth)
        {
            UCHAR curDir = way[way.size() - 1].getDir();
            if (curDir == 5
                || (way.size() > 4) && (way[way.size() - 1].pos == way[way.size() - 5].pos)
                || (way.size() > 6) && (way[way.size() - 1].pos == way[way.size() - 7].pos))
            {
                if (way[way.size() - 1].enemy == NONE && way.size() > 1)
                {
                    cell* temp = new cell[32];
                    memcpy(temp, field, sizeof(field));
                    for (int i = 0; i < way.size() - 1; i++)
                    {
                        temp[way[i].pos] = EMPTINESS;
                        temp[way[i].enemy] = EMPTINESS;
                    }
                    temp[way[way.size() - 1].pos] = way[way.size() - 1].type;
                    way.pop_back();
                    result.push_back(temp);
                }
                else
                {
                    way.pop_back();
                }
                continue;
            }

            /* Ищем шашку противника */

            UCHAR curEnemy = link[way[way.size() - 1].pos][curDir];
            if (curEnemy == NONE)
            {
                continue;
            }
            UCHAR newPos = link[curEnemy][curDir];
            while (true)
            {
                if (curEnemy == NONE || newPos == NONE)
                {
                    break;
                }
                
                if (way.size() > (curDepth + 1))
                {
                    int i = 1;
                    bool flag = false;
                    while (true)
                    {
                        i++;
                        if (curEnemy == way[way.size() - i].enemy)
                        {
                            flag = true;
                            break;
                        }
                        if (i == way.size())
                        {
                            break;
                        }
                    }
                    if (flag)
                    {
                        break;
                    }
                }
                
                if (canEat(field[curEnemy], turn) && (field[newPos] == EMPTINESS))
                {
                    curEnemy = link[curEnemy][curDir];
                    while (field[curEnemy] == EMPTINESS)
                    {
                        cell type = way[way.size() - 1].type;
                        way[way.size() - 1].enemy = curEnemy;
                        if (way.size() != curDepth + 1)
                        {
                            way.push_back(WayPoint(link[curEnemy][curDir], type));
                        }
                    }
                    break;
                }
                curEnemy = newPos;
                newPos = link[newPos][curDir];
            }
        }
    }
    else
    {
        for (int i = 0; i < 4; i++)
        {
            UCHAR new_pos = pos;
            while (true)
            {
                new_pos = link[new_pos][i];
                if (new_pos == NONE)
                    break;
                if (field[new_pos] != EMPTINESS)
                    break;
                cell *temp = new cell[32];
                memcpy(temp, field, sizeof(cell)* 32);
                temp[new_pos] = field[pos];
                temp[pos] = EMPTINESS;
                result.push_back(temp);
            }
        }
    }
	/*UCHAR pos1;
	if (eat)
	{
		vector<UCHAR> way;
		for (int i = 0; i < 4; i++)
		{
			pos1 = link[pos][i];
			if (pos1 == NONE)
				continue;
			while (field[pos1] == EMPTINESS)
			{
				pos1 = link[pos1][i];
				if (pos1 == NONE)
					break;
			}
			if (pos1 == NONE)
				continue;
			if (canEat(field[link[pos1][i]], turn))
			{
				if (link[link[pos1][i]][i] == NONE)
					continue;
				UCHAR new_pos = link[link[pos1][i]][i];
				if (field[new_pos] == EMPTINESS)
				{
					way.push_back(pos1);
					way.push_back(link[pos1][i]);
					way.push_back(new_pos);
					queenRecursive(result, way);
				}
			}
			way.clear();
		}
	}
	else
	{
		for (int i=0; i < 4; i++)
		{
			UCHAR new_pos = pos;
			while (true)
			{
				new_pos = link[new_pos][i];
				if (new_pos == NONE)
					break;
				if (field[new_pos] != EMPTINESS)
					break;
				cell *temp = new cell[32];
				memcpy(temp, field, sizeof(cell)* 32);
				temp[new_pos] = field[pos];
				temp[pos] = EMPTINESS;
				result.push_back(temp);
			}
		}
	}*/
	return result;
}

/*void checkerboard::queenRecursive(vector<cell*> &result, vector<UCHAR> way)
{
	bool end = true;
	UCHAR curr = way[way.size() - 1];
	UCHAR prev = way[way.size() - 2];
	for (int i = 0; i < 4; i++)
	{
		if (link[curr][i] == NONE)
			continue;
		if (link[curr][i] != prev && canEat(field[link[curr][i]], turn))
		{
			if (link[link[curr][i]][i] == NONE)
				continue;
			UCHAR new_pos = link[link[curr][i]][i];
			if (field[new_pos] == EMPTINESS)
			{
				way.push_back(link[curr][i]);
				way.push_back(new_pos);
				queenRecursive(result, way);
				end = false;
				way.pop_back();
				way.pop_back();
			}
		}
	}
	if (end)
	{
		cell* temp = new cell[32];
		memcpy(temp, field, sizeof(field));
		temp[curr] = turn ? BLACKQUEEN : WHITEQUEEN;
		way.pop_back();
		while (way.size() > 0)
		{
			temp[way[way.size() - 1]] = EMPTINESS;
			way.pop_back();
		}
		result.push_back(temp);
	}
}*/

int checkerboard::getDivide(bool player)
{
    int countW = 0, countB = 0, countWQ = 0, countBQ = 0;
    int *mark = new int[32]();
    int lastMark = 0;
    for (int i = 0; i < 32; i++)
    {
        if (field[i] == WHITE)
        {
            countW++;
        }
        if (field[i] == BLACK)
        {
            countB++;
        }
        if (field[i] == WHITEQUEEN)
        {
            countWQ++;
        }
        if (field[i] == BLACKQUEEN)
        {
            countBQ++;
        }

        // Хочу в дамки
        if ((i >= 16) && (field[i] == WHITE) && !player
            || (i < 16) && (field[i] == BLACK) && player)
        {
            mark[i] = (i / 4 - 3) * 2;
        }

        // Не хочу, чтобы у противника была дамка
        if ((i >= 16) && (field[i] == WHITE) && player
            || (i < 16) && (field[i] == BLACK) && !player)
        {
            mark[i] = - (i / 4 - 3) * 2;
        }

        // Хочу быть в центре, там свободно
        if (((i > 12) && (i < 15)) || ((i > 16) && (i < 19))
            && ((field[i] == WHITE) && !player || (field[i] == BLACK) && player))
        {
            mark[i] += 4;
        }

        // Не хочу видеть противника в центре
        if (((i > 12) && (i < 15)) || ((i > 16) && (i < 19))
            && ((field[i] == WHITE) && player || (field[i] == BLACK) && !player))
        {
            mark[i] -= 4;
        }
        
        lastMark += mark[i];
    }

    if (player)
    {
        lastMark += (countB - countW) * 20 + (countBQ - countWQ) * 40;
		//lastMark = countB - countW;
    }
    else
    {
        lastMark -= (countB - countW) * 20 + (countBQ - countWQ) * 40;
		//lastMark = - countB + countW;
    }

    return lastMark;
}

cell checkerboard :: get(int x, int y)
{
    if (x < 0 || y < 0 || x > 7 || y > 7 || (((x + y) % 2) == 1))
    {
        return EMPTINESS;
    }
    return (field[x/2 + y*4]);
}