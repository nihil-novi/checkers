#include "GUI.h"
#include <stdio.h>
#include <string.h>
#include <Windows.h>

GUI::GUI(
    unsigned int winSide_)
{
    winSide = winSide_;
    rectSide = winSide_ / 10;
	chosenChecker = new SDL_Rect();
	chosenChecker->h = rectSide;
	chosenChecker->w = rectSide;
	chosenChecker->x = -1;
	chosenChecker->y = -1;
    window = nullptr;
    render = nullptr;
    textureBackgr = nullptr;
    textureBlack = nullptr;
    texture123 = nullptr;
    textureABC = nullptr;
    textureCheckBlack = nullptr;
    textureCheckWhite = nullptr;
    textureQueenBlack = nullptr;
    textureQueenWhite = nullptr;
    textureWhite = nullptr;
    init();
}

GUI::~GUI()
{
    destroy();
}

void GUI::loadTexForMainMenu(
    SDL_Texture *&background,
    SDL_Texture *&play,
    SDL_Texture *&white,
    SDL_Texture *&black,
    SDL_Texture *&man,
    SDL_Texture *&AI,
    SDL_Texture *&exit,
    SDL_Texture *&arrow)
{
    //Background
    SDL_Surface *pic = SDL_LoadBMP("../../Textures/0.bmp");
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    background = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (background == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    //Play
    pic = SDL_LoadBMP("../../Textures/play.bmp");
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    play = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (play == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    //White
    pic = SDL_LoadBMP("../../Textures/fieldWhite.bmp");
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    white = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (white == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    //Black
    pic = SDL_LoadBMP("../../Textures/fieldBlack.bmp");
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    black = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (black == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    //WhitePlayer
    pic = SDL_LoadBMP("../../Textures/man.bmp");
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    man = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (man == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    //BlackPlayer
    pic = SDL_LoadBMP("../../Textures/AI.bmp");
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    AI = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (AI == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    //Arrow
    pic = SDL_LoadBMP("../../Textures/arrow.bmp");
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    arrow = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (arrow == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    //Exit
    pic = SDL_LoadBMP("../../Textures/exit.bmp");
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    exit = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (exit == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }
}

void GUI::mainMenu()
{
    Player playerBlack = COMPUTER, playerWhite = MAN;

    // Prepare textures

    SDL_Texture *background = nullptr, *play = nullptr,
        *white = nullptr, *black = nullptr, **player_ = nullptr,
        *exit_ = nullptr, *arrow = nullptr;

    player_ = new SDL_Texture*[2];
    player_[0] = nullptr;
    player_[1] = nullptr;

    loadTexForMainMenu(
        background,
        play,
        white,
        black,
        player_[MAN],
        player_[COMPUTER],
        exit_,
        arrow);

    bool flag = true, canExit = false;
    SDL_Event wEvent;
    Button curButton = PLAY;

    //Prepare rectangles
    SDL_Rect backRect, playRect, playerRect, arrowRect;
    backRect.w = winSide;
    backRect.h = winSide;
    backRect.x = 0;
    backRect.y = 0;

    playRect.w = winSide / 3;
    playRect.h = winSide / 6;
    playRect.x = winSide / 3;
    playRect.y = playRect.h;

    arrowRect.w = playRect.w;
    arrowRect.h = playRect.h;
    arrowRect.x = playRect.x;
    arrowRect.y = playRect.y;

    playerRect.w = playRect.w;
    playerRect.h = playRect.h;
    playerRect.x = playRect.w * 2;
    playerRect.y = 2 * playRect.h;

    while (flag)
    {
        while (SDL_PollEvent(&wEvent))
        {
            switch (wEvent.type)
            {
            case SDL_KEYDOWN:
                switch (wEvent.key.keysym.sym)
                {
                case SDLK_DOWN:
                    curButton = (Button)((curButton + 1) % 4);
                    break;
                case SDLK_UP:
                    curButton = (Button)((curButton + 3) % 4);
                    break;
                case SDLK_RETURN:
                case SDLK_KP_ENTER:
                    switch (curButton)
                    {
                    case PLAY:
                        flag = false;
                        break;
                    case WHITEPLAYER:
                        playerWhite = (Player)((playerWhite + 1) % 2);
                        break;
                    case BLACKPLAYER:
                        playerBlack = (Player)((playerBlack + 1) % 2);
                        break;
                    case EXIT:
                        flag = false;
                        canExit = true;
                        break;
                    }
                    break;
                case SDLK_ESCAPE:
                    flag = false;
                    canExit = true;
                    break;
                }
                break;
            case SDL_QUIT:
                flag = false;
                canExit = true;
                break;
            }
        }
        SDL_RenderClear(render);
        SDL_RenderCopy(render, background, NULL, &backRect);
        SDL_RenderCopy(render, play, NULL, &playRect);
        playRect.y += playRect.h;
        SDL_RenderCopy(render, white, NULL, &playRect);
        playRect.y += playRect.h;
        SDL_RenderCopy(render, black, NULL, &playRect);
        playRect.y += playRect.h;
        SDL_RenderCopy(render, exit_, NULL, &playRect);
        SDL_RenderCopy(render, player_[playerWhite], NULL, &playerRect);
        playerRect.y += playerRect.h;
        SDL_RenderCopy(render, player_[playerBlack], NULL, &playerRect);
        arrowRect.y = ((int)curButton + 1) * playRect.h;
        SDL_RenderDrawRect(render, &arrowRect);

        playRect.y -= 3 * playRect.h;
        playerRect.y -= playerRect.h;

        SDL_RenderPresent(render);
    }

    player = new Player[2];
    player[1] = playerBlack;
    player[0] = playerWhite;

    SDL_DestroyTexture(background);
    SDL_DestroyTexture(play);
    SDL_DestroyTexture(white);
    SDL_DestroyTexture(black);
    SDL_DestroyTexture(player_[0]);
    SDL_DestroyTexture(player_[1]);
    delete[] player_;
    SDL_DestroyTexture(arrow);
    SDL_DestroyTexture(exit_);

    if (canExit)
    {
        destroy();
        exit(0);
    }
}

void GUI::init()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
        printf("Error %s", SDL_GetError());
    }

    window = SDL_CreateWindow("Checkers", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        winSide, winSide, SDL_WINDOW_SHOWN);
    if (window == nullptr)
    {
        printf("Error %s", SDL_GetError());
    }

    render = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (render == nullptr)
    {
        printf("Error %s", SDL_GetError());
    }

    fileABC = new char*[8];
    file123 = new char*[8];
    textureABC = new SDL_Texture*[8];
    texture123 = new SDL_Texture*[8];
    for (int i = 0; i < 8; i++)
    {
        texture123[i] = nullptr;
        textureABC[i] = nullptr;
    }
    loadTextures();
    mainMenu();
}

void GUI::loadTextures()
{
    //Background
    SDL_Surface *pic = SDL_LoadBMP("../../Textures/0.bmp");
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    textureBackgr = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (textureBackgr == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    // Black

    pic = SDL_LoadBMP(fileTexBlack);
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    textureBlack = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (textureBlack == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    // White

    pic = SDL_LoadBMP(fileTexWhite);
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    textureWhite = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (textureWhite == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    // Black check

    pic = SDL_LoadBMP(fileTexCheckBlack);
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    textureCheckBlack = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (textureCheckBlack == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    // White check

    pic = SDL_LoadBMP(fileTexCheckWhite);
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    textureCheckWhite = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (textureCheckWhite == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    // Black queen
    
    pic = SDL_LoadBMP(fileTexQueenWhite);
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    textureQueenWhite = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (textureQueenWhite == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    // White queen

    pic = SDL_LoadBMP(fileTexQueenBlack);
    if (pic == nullptr)
    {
        printf("SDL_LoadBMP Error: %s", SDL_GetError());
    }
    textureQueenBlack = SDL_CreateTextureFromSurface(render, pic);
    SDL_FreeSurface(pic);
    if (textureQueenBlack == nullptr)
    {
        printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
    }

    // ABC

    for (int i = 0; i < 8; i++)
    {
        fileABC[i] = new char[21];
        fileABC[i][0] = '\0';
        strcat(fileABC[i], "../../Textures/");
        fileABC[i][15] = i + 'a';
        fileABC[i][16] = '.';
        fileABC[i][17] = 'b';
        fileABC[i][18] = 'm';
        fileABC[i][19] = 'p';
        fileABC[i][20] = '\0';
        
        pic = SDL_LoadBMP(fileABC[i]);
        if (pic == nullptr)
        {
            printf("SDL_LoadBMP Error: %s", SDL_GetError());
        }
        textureABC[i] = SDL_CreateTextureFromSurface(render, pic);
        SDL_FreeSurface(pic);
        if (textureABC[i] == nullptr)
        {
            printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
        }
    }

    // 123

    for (int i = 0; i < 8; i++)
    {
        file123[i] = new char[21];
        file123[i][0] = '\0';
        strcat(file123[i], "../../Textures/");
        file123[i][15] = (char)(i + 1) + '0';
        file123[i][16] = '.';
        file123[i][17] = 'b';
        file123[i][18] = 'm';
        file123[i][19] = 'p';
        file123[i][20] = '\0';
        pic = SDL_LoadBMP(file123[i]);
        if (pic == nullptr)
        {
            printf("SDL_LoadBMP Error: %s", SDL_GetError());
        }
        texture123[i] = SDL_CreateTextureFromSurface(render, pic);
        SDL_FreeSurface(pic);
        if (texture123[i] == nullptr)
        {
            printf("SDL_CreateTextureFromSurface Error: %s", SDL_GetError());
        }
    }
}

void GUI::destroy()
{
    SDL_DestroyTexture(textureBlack);
    SDL_DestroyTexture(textureCheckBlack);
    SDL_DestroyTexture(textureCheckWhite);
    SDL_DestroyTexture(textureQueenBlack);
    SDL_DestroyTexture(textureQueenWhite);
    SDL_DestroyTexture(textureWhite);
    for (int i = 0; i < 8; i++)
    {
        SDL_DestroyTexture(texture123[i]);
        SDL_DestroyTexture(textureABC[i]);
        delete[] file123[i];
        delete[] fileABC[i];
    }
    delete[] file123;
    delete[] fileABC;
    delete[] texture123;
    delete[] textureABC;

    delete[] player;
	delete[] chosenChecker;

    SDL_DestroyRenderer(render);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void GUI::drawTable(checkerboard *board)
{
    SDL_RenderClear(render);
    SDL_Rect rect;
    rect.x = 0;
    rect.y = 0;
    rect.h = winSide;
    rect.w = winSide;
    SDL_RenderCopy(render, textureBackgr, NULL, &rect);
    
    SDL_Rect imgRect;

    imgRect.h = rectSide;
    imgRect.w = rectSide;
    imgRect.x = 0;
    imgRect.y = winSide - 2 * imgRect.h;

    // Draw numbers
    for (int i = 0; i < 8; i++)
    {
        SDL_RenderCopy(render, texture123[i], NULL, &imgRect);
        imgRect.y -= imgRect.h;
    }

    // Draw letters
    imgRect.y = winSide - imgRect.h;
    imgRect.x = imgRect.w;
    for (int i = 0; i < 8; i++)
    {
        SDL_RenderCopy(render, textureABC[i], NULL, &imgRect);
        imgRect.x += imgRect.w;
    }

    // Draw fields
    imgRect.y = winSide - 2 * imgRect.h;
    for (int i = 1; i < 9; i++)
    {
        imgRect.x = imgRect.w;
        for (int j = 1; j < 9; j++)
        {
            if ((i + j) % 2)
            {
                SDL_RenderCopy(render, textureWhite, NULL, &imgRect);
            }
            else
            {
                SDL_RenderCopy(render, textureBlack, NULL, &imgRect);
            }
            imgRect.x += imgRect.w;
        }
        imgRect.y -= imgRect.h;
    }
    drawCheckers(board);
    SDL_RenderPresent(render);
}

void GUI::drawCheckers(checkerboard *board)
{
    SDL_Rect rect;
    rect.h = rectSide;
    rect.w = rectSide;

    rect.y = winSide - 2 * rect.h;
    for (int i = 1; i < 9; i++)
    {
        rect.x = rect.w;
        for (int j = 1; j < 9; j++)
        {
            switch (board->get(j - 1, i - 1))
            {
            case EMPTINESS: break;
            case WHITE:
                SDL_RenderCopy(render, textureCheckWhite, NULL, &rect);
                break;
            case WHITEQUEEN:
                SDL_RenderCopy(render, textureQueenWhite, NULL, &rect);
                break;
            case BLACK:
                SDL_RenderCopy(render, textureCheckBlack, NULL, &rect);
                break;
            case BLACKQUEEN:
                SDL_RenderCopy(render, textureQueenBlack, NULL, &rect);
                break;
            }
            rect.x += rect.w;
        }
        rect.y -= rect.h;
    }
	if (chosenChecker->x != -1 && chosenChecker->y != -1)
	{
		SDL_SetRenderDrawColor(render, 10, 10, 255, 255);
		SDL_RenderDrawRect(render, chosenChecker);
	}
}

bool GUI::events(checkerboard *&board)
{
    static int counter;
    static Turn *turn;
    SDL_Event wEvent;

    if (player[counter] == COMPUTER)
    {
        counter = (counter + 1) % 2;
        and_or_tree* tree = new and_or_tree(board, 5);
        board = tree->returnTurn();
        if (board == NULL)
            exit(1);
        delete tree;
    }
    while (SDL_PollEvent(&wEvent))
    {
        switch (wEvent.type)
        {
        case SDL_MOUSEBUTTONUP:
            if (player[counter] == MAN)
            {
                static int click;
                if (click == 0)
                {
                    turn = new Turn;
                    turn->x1 = wEvent.button.x / rectSide;
                    turn->y1 = (winSide - wEvent.button.y) / rectSide;
                    if (turn->checkFirst(board))
                    {
                        click = (click + 1) % 2;
						setChosenChecker((wEvent.button.x / rectSide) * rectSide, (wEvent.button.y / rectSide) * rectSide);
                    }
                }
				else
                {
                    if ((turn->x1 == wEvent.button.x / rectSide)
                        &&
                        (turn->y1 == (winSide - wEvent.button.y) / rectSide))
                    {
                        click = (click + 1) % 2;
						setChosenChecker();
                        delete turn;
                    }
                    else
                    {
                        turn->x2 = wEvent.button.x / rectSide;
                        turn->y2 = (winSide - wEvent.button.y) / rectSide;
                        if (turn->checkSecond(board))
                        {
                            counter = (counter + 1) % 2;
                            click = (click + 1) % 2;
							setChosenChecker();
                            delete turn;
                        }
                    }
                }
            }
            break;
        case SDL_KEYDOWN:
            switch (wEvent.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                return false;
            }
            break;
        case SDL_QUIT:
            return false;
        }
    }
    return true;
}

bool Turn::checkFirst(checkerboard *board)
{
    if (((board->get(x1 - 1, y1 - 1) == WHITE)
        || (board->get(x1 - 1, y1 - 1) == WHITEQUEEN)) && !board->turn
        || ((board->get(x1 - 1, y1 - 1) == BLACK)
        || (board->get(x1 - 1, y1 - 1) == BLACKQUEEN) && board->turn))
    {
        return true;
    }
    return false;
}

bool Turn::checkSecond(checkerboard *&board)
{
    checkerboard* temp = new checkerboard(*board);
    node *x = new node(temp, temp->turn);
    
    x->discover(temp->turn);
    node *tmp;
    while (tmp = x->getNextChild())
    {
        if ((tmp->state->get(x1 - 1, y1 - 1) == EMPTINESS)
			&& ((tmp->state->get(x2 - 1, y2 - 1) == board->get(x1 - 1, y1 - 1)) && board->get(x2 - 1, y2 - 1) != board->get(x1 - 1, y1 - 1) ||
			temp->turn && tmp->state->get(x2 - 1, y2 - 1) == BLACKQUEEN && y2==1 && temp->get(x2-1,y2-1)!=BLACKQUEEN|| 
			!temp->turn && tmp->state->get(x2 - 1, y2 - 1) == WHITEQUEEN && y2 == 8 && temp->get(x2 - 1, y2 - 1) != WHITEQUEEN))
			/* TODO �������� ���������� ������ �� ����������� �������� ����� ����� ����� ������*/
        {
            delete board;
            board = new checkerboard(*(tmp->state));
            delete x;
            return true;
        }
    }
    delete x;
    return false;
}

void GUI::setChosenChecker(int x_, int y_)
{
	chosenChecker->x = x_;
	chosenChecker->y = y_;
}