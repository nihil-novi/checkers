#include <SDL.h>
#include "tree.h"

enum Button : unsigned char
{
    PLAY = 0,
    WHITEPLAYER,
    BLACKPLAYER,
    EXIT
};

enum Player : unsigned char
{
    COMPUTER = 0,
    MAN = 1
};

struct Turn
{
    unsigned int x1, y1;
    unsigned int x2, y2;
    bool checkFirst(checkerboard *board);
    bool checkSecond(checkerboard *&board);
};

class GUI
{
private:
    unsigned int winSide;
    unsigned int rectSide;
    SDL_Window *window;
    SDL_Renderer *render;
    SDL_Texture *textureBackgr;
    SDL_Texture *textureBlack, *textureWhite;
    SDL_Texture *textureCheckBlack, *textureCheckWhite;
    SDL_Texture *textureQueenBlack, *textureQueenWhite;
    SDL_Texture **textureABC, **texture123;
	SDL_Rect *chosenChecker;
    const char *fileTexBlack = "../../Textures/BLACK.bmp";
    const char *fileTexWhite = "../../Textures/WHITE.bmp";
    const char *fileTexCheckBlack = "../../Textures/blackCheck.bmp";
    const char *fileTexCheckWhite = "../../Textures/whiteCheck.bmp";
    const char *fileTexQueenWhite = "../../Textures/whiteQueen.bmp";
    const char *fileTexQueenBlack = "../../Textures/blackQueen.bmp";
    char **fileABC, **file123;
    Player *player;
    void loadTextures();
    void init();
    void mainMenu();
    void loadTexForMainMenu(
        SDL_Texture *&background,
        SDL_Texture *&play,
        SDL_Texture *&white,
        SDL_Texture *&black,
        SDL_Texture *&whitePlayer,
        SDL_Texture *&blackPlayer,
        SDL_Texture *&exit,
        SDL_Texture *&arrow);
public:
    GUI(
        unsigned int winSide_ = 600
        );
    ~GUI();
    void drawTable(checkerboard *board);
    void drawCheckers(checkerboard *board);
    bool events(checkerboard *&board);
	void setChosenChecker(int x_ = -1, int y_ = -1);
    void destroy();
};