#include <vector>
#include <stack>
#include "link.h"

using namespace std;

enum cell : unsigned char
{
	EMPTINESS,
	WHITE,
	BLACK,
	WHITEQUEEN,
	BLACKQUEEN
};

class WayPoint
{
private:
    UCHAR counter;
public:
    UCHAR pos;
    cell type;
    UCHAR enemy;
    WayPoint(UCHAR pos_ = 0, cell type_ = EMPTINESS)
        :
        pos(pos_), counter(0), type(type_), enemy(NONE) {}
    UCHAR getDir()
    {
        UCHAR tmp = counter;
        counter++;
        return tmp;
    }
};

class checkerboard
{
private:
	bool eat; // 0 - unavailable to eat, 1 - available to eat
	void checkEat();
	vector<cell*> checkerTurn(UCHAR pos);
	vector<cell*> queenTurn(UCHAR pos, vector<WayPoint> *way_);
public:
	cell field[32];
	bool turn; // 0 - WHITE, 1 - BLACK
	checkerboard();
	checkerboard(const checkerboard &op);
	checkerboard(cell* _field, bool _turn);
	vector<checkerboard*> getTurns();
    int getDivide(bool player);
    cell get(int x, int y);
};